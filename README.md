# android_vendor_xiaomi_psyche-firmware

Firmware images for Xiaomi 12 X (psyche), to include in custom ROM builds.

**Current version**:fw_psyche_miui_PSYCHEEEAGlobal_V13.0.3.0.SLDEUXM_7b29cba800_12.0.zip

### How to use?

1. Clone this repo to `vendor/xiaomi/psyche-firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/psyche-firmware/BoardConfigVendor.mk
```
